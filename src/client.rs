use crate::codec::{SyncDrawCodec, SyncDrawCodecError};
use crate::message::{Msg, SharedMessage};
use crate::network::Network;
use crate::protocol;

use std::net::SocketAddr;
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

use futures::SinkExt;
use tokio::net::TcpStream;
use tokio::stream::Stream;
use tokio::sync::{mpsc, Mutex};
use tokio_util::codec::Framed;

type Rx = mpsc::Receiver<SharedMessage>;

pub struct Client {
    pub name: String,
    io_interface: Framed<TcpStream, SyncDrawCodec>,
    rx: Rx,
}

impl Client {
    pub async fn new(
        addr: SocketAddr,
        io_interface: Framed<TcpStream, SyncDrawCodec>,
        state: Arc<Mutex<Network>>,
    ) -> Client {
        let (tx, rx) = mpsc::channel(1000);
        let mut network = state.lock().await;
        network.add_client(addr, tx).await;

        Client {
            name: network.next_name(),
            io_interface,
            rx,
        }
    }

    pub async fn greet(&mut self, state: Arc<Mutex<Network>>) -> Result<(), SyncDrawCodecError> {
        let network = state.lock().await;
        let msg = protocol::create_greeting(self, &network);
        self.send(msg).await
    }

    pub async fn send(&mut self, data: SharedMessage) -> Result<(), SyncDrawCodecError> {
        self.io_interface.send(data).await
    }
}

impl Stream for Client {
    type Item = Result<Msg, SyncDrawCodecError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        // Check if we have pending messages that need to be sent to the client
        if let Poll::Ready(Some(data)) = Pin::new(&mut self.rx).poll_next(cx) {
            return Poll::Ready(Some(Ok(Msg::Received(data))));
        }

        // Poll the input stream
        let msg = futures::ready!(Pin::new(&mut self.io_interface).poll_next(cx));
        Poll::Ready(match msg {
            // The client sent something, let's broadcast it
            Some(Ok(data)) => Some(Ok(Msg::Broadcast(data))),
            // Error
            Some(Err(e)) => Some(Err(e)),
            // Exhausted stream
            None => None,
        })
    }
}

use std::net::IpAddr;

use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(author = "jesdazrez")]
pub struct Args {
    #[structopt(short, long, default_value = "127.0.0.1")]
    pub hostname: IpAddr,
    #[structopt(short, long, default_value = "57135")]
    pub port: u16,
}

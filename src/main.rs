mod cli_opts;
mod client;
mod codec;
mod message;
mod network;
mod protocol;
mod server;

use cli_opts::Args;
use network::Network;

use std::error::Error;
use std::net::SocketAddr;
use std::sync::Arc;

use futures::{future::FutureExt, select};
use structopt::StructOpt;
use tokio::net::TcpListener;
use tokio::sync::{oneshot, Mutex};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Parse and validate args
    let args = Args::from_args();

    // Create a shutdown switch and fuse it so we can select! on it later
    let (shutdown_switch, exit_signal) = oneshot::channel::<()>();
    let mut exit_signal = exit_signal.fuse();

    // Create the network and give it the shutdown switch
    let state = Arc::new(Mutex::new(Network::new(shutdown_switch)));

    // Init the TCP server
    let mut listener = TcpListener::bind(SocketAddr::new(args.hostname, args.port)).await?;
    println!("SyncDraw server started on {}:{}", args.hostname, args.port);

    // Handle new connections until the exit signal is received
    loop {
        let next_conn = select! {
            new_conn = listener.accept().fuse() => Some(new_conn),
            should_exit = exit_signal => None
        };

        match next_conn {
            Some(Ok((stream, addr))) => {
                tokio::spawn(server::handle_connection(stream, addr, Arc::clone(&state)));
            }
            Some(Err(e)) => {
                eprintln!("An error occurred while accepting a new connection!");
                eprintln!("Error = {:?}", e);
            }
            None => {
                break;
            }
        };
    }

    // Exit when all the clients are gone
    println!("Shutting down...");
    Ok(())
}

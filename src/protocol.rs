#[allow(dead_code, unused_imports)]
#[allow(
    clippy::extra_unused_lifetimes,
    clippy::needless_lifetimes,
    clippy::redundant_closure,
    clippy::redundant_field_names,
    clippy::redundant_static_lifetimes,
    clippy::trivially_copy_pass_by_ref
)]
mod interface_generated;

pub use interface_generated::{get_size_prefixed_root_as_message, Payload as PayloadType};
use interface_generated::{
    Hello, HelloArgs, Message, MessageArgs, UpdateNetSizeCmd, UpdateNetSizeCmdArgs,
};

use crate::client::Client;
use crate::message::{OwnedMessage, SharedMessage};
use crate::network::Network;

use std::sync::Arc;

use flatbuffers::FlatBufferBuilder;

pub fn create_greeting(client: &Client, network: &Network) -> SharedMessage {
    let mut builder = FlatBufferBuilder::new();
    let name = builder.create_string(&client.name);
    let window_size = network.window_current_size();
    let payload = Hello::create(
        &mut builder,
        &HelloArgs {
            name: Some(name),
            width: window_size.0,
            height: window_size.1,
            net_size: network.size() as u32,
        },
    );
    let msg = Message::create(
        &mut builder,
        &MessageArgs {
            payload_type: PayloadType::Hello,
            payload: Some(payload.as_union_value()),
        },
    );

    builder.finish_size_prefixed(msg, None);
    let (mut data, start) = builder.collapse();
    data.drain(..start);

    Arc::new(OwnedMessage {
        payload_type: PayloadType::Hello,
        buffer: data[..].into(),
    })
}

pub fn create_size_updated_msg(new_size: usize) -> SharedMessage {
    let mut builder = FlatBufferBuilder::new();
    let payload = UpdateNetSizeCmd::create(
        &mut builder,
        &UpdateNetSizeCmdArgs {
            size_: new_size as u32,
        },
    );
    let msg = Message::create(
        &mut builder,
        &MessageArgs {
            payload_type: PayloadType::UpdateNetSizeCmd,
            payload: Some(payload.as_union_value()),
        },
    );

    builder.finish_size_prefixed(msg, None);
    let (mut data, start) = builder.collapse();
    data.drain(..start);

    Arc::new(OwnedMessage {
        payload_type: PayloadType::UpdateNetSizeCmd,
        buffer: data[..].into(),
    })
}

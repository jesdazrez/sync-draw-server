use crate::protocol::PayloadType;

use std::sync::Arc;

use bytes::BytesMut;

pub struct OwnedMessage {
    pub payload_type: PayloadType,
    pub buffer: BytesMut,
}

pub type SharedMessage = Arc<OwnedMessage>;

pub enum Msg {
    Received(SharedMessage),
    Broadcast(OwnedMessage),
}

use crate::client::Client;
use crate::codec::SyncDrawCodec;
use crate::message::Msg;
use crate::network::Network;

use std::net::SocketAddr;
use std::sync::Arc;

use tokio::net::TcpStream;
use tokio::stream::StreamExt;
use tokio::sync::Mutex;
use tokio_util::codec::Framed;

pub async fn handle_connection(stream: TcpStream, addr: SocketAddr, state: Arc<Mutex<Network>>) {
    // Register client in the network
    let mut client = Client::new(
        addr,
        Framed::new(stream, SyncDrawCodec::new()),
        Arc::clone(&state),
    )
    .await;
    // Greet client
    if let Err(e) = client.greet(Arc::clone(&state)).await {
        eprintln!("Could not greet {}", client.name);
        eprintln!("Error = {:?}", e);
    } else {
        println!("{} connected!", client.name);
    }

    while let Some(res) = client.next().await {
        match res {
            Ok(Msg::Broadcast(data)) => {
                let mut network = state.lock().await;
                network.broadcast(&addr, Arc::new(data)).await;
            }
            Ok(Msg::Received(data)) => {
                if let Err(e) = client.send(data).await {
                    eprintln!("Could not send data to {}!", client.name);
                    eprintln!("Error = {:?}", e);
                }
            }
            Err(e) => {
                eprintln!("The connection with {} was lost...", client.name);
                eprintln!("Error = {:?}", e);
                break;
            }
        }
    }

    {
        let mut network = state.lock().await;
        network.remove_client(&addr).await;
        println!("{} disconnected!", client.name);
    }
}

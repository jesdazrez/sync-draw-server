use crate::message::SharedMessage;
use crate::protocol::{create_size_updated_msg, get_size_prefixed_root_as_message, PayloadType};

use std::collections::HashMap;
use std::net::SocketAddr;
use std::sync::Arc;

use tokio::sync::{mpsc, oneshot};

type Tx = mpsc::Sender<SharedMessage>;

pub struct Network {
    shutdown_switch: Option<oneshot::Sender<()>>,
    clients: HashMap<SocketAddr, Tx>,
    names: Box<dyn Iterator<Item = String> + Send>,
    window_current_size: (u32, u32),
}

impl Network {
    pub fn new(shutdown_switch: oneshot::Sender<()>) -> Network {
        Network {
            shutdown_switch: Some(shutdown_switch),
            clients: HashMap::new(),
            // This will generate A, B, ..., Z, AA, BB, ..., ZZ, AAA, ...
            names: Box::new(
                (1..)
                    .zip(std::iter::repeat((b'A'..=b'Z').map(char::from)))
                    .flat_map(|(n, alphabet)| {
                        alphabet.map(move |letter| {
                            std::iter::repeat(letter).take(n).collect::<String>()
                        })
                    }),
            ),
            window_current_size: (0, 0),
        }
    }

    pub fn size(&self) -> usize {
        self.clients.len()
    }

    pub fn next_name(&mut self) -> String {
        // Unwrapping here is safe because the names iterator is infinite
        self.names.next().unwrap()
    }

    pub fn window_current_size(&self) -> (u32, u32) {
        self.window_current_size
    }

    pub async fn add_client(&mut self, addr: SocketAddr, tx: Tx) {
        // Add to the collection
        self.clients.insert(addr, tx);
        // Notify all other clients
        self.broadcast_size_updated(&addr).await;
    }

    pub async fn remove_client(&mut self, addr: &SocketAddr) {
        // Remove from the collection
        self.clients.remove(addr);
        // Notify all other clients or shut down the server
        if self.clients.is_empty() {
            self.shutdown_switch.take();
        } else {
            self.broadcast_size_updated(&addr).await;
        }
    }

    pub async fn broadcast(&mut self, sender: &SocketAddr, data: SharedMessage) {
        if data.payload_type == PayloadType::ResizeCmd {
            let msg = get_size_prefixed_root_as_message(&data.buffer);
            if let Some(payload) = msg.payload_as_resize_cmd() {
                self.window_current_size = (payload.width(), payload.height())
            }
        }

        self.broadcast_without_inspect(sender, data).await;
    }

    async fn broadcast_without_inspect(&mut self, sender: &SocketAddr, data: SharedMessage) {
        for (addr, tx) in self.clients.iter_mut() {
            if addr != sender {
                if tx.send(Arc::clone(&data)).await.is_err() {
                    eprintln!("Could not broadcast to: {}!", addr);
                }
            }
        }
    }

    async fn broadcast_size_updated(&mut self, sender: &SocketAddr) {
        let msg = create_size_updated_msg(self.size());
        self.broadcast_without_inspect(&sender, msg).await;
    }
}

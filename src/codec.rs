use crate::message::{OwnedMessage, SharedMessage};
use crate::protocol::{get_size_prefixed_root_as_message, PayloadType};

use std::io;

use bytes::BytesMut;
use tokio_util::codec::{Decoder, Encoder};

#[derive(Debug)]
pub enum SyncDrawCodecError {
    InvalidPayload,
    IO(io::Error),
}

impl From<io::Error> for SyncDrawCodecError {
    fn from(err: io::Error) -> SyncDrawCodecError {
        SyncDrawCodecError::IO(err)
    }
}

pub struct SyncDrawCodec {
    curr_frame_size: Option<usize>,
    buffer: BytesMut,
}

impl SyncDrawCodec {
    const FRAME_PREFIX_SIZE: usize = flatbuffers::SIZE_SIZEPREFIX;

    pub fn new() -> Self {
        SyncDrawCodec {
            curr_frame_size: None,
            buffer: BytesMut::new(),
        }
    }

    fn try_init_next_frame(&mut self) {
        if self.buffer.len() >= SyncDrawCodec::FRAME_PREFIX_SIZE {
            let payload_size = flatbuffers::read_scalar::<u32>(&self.buffer) as usize;
            self.curr_frame_size = Some(payload_size + SyncDrawCodec::FRAME_PREFIX_SIZE);
        }
    }

    fn process_frame(&mut self, frame_size: usize) -> Result<OwnedMessage, SyncDrawCodecError> {
        let frame_bytes = self.buffer.split_to(frame_size);
        self.curr_frame_size = None;

        let msg = get_size_prefixed_root_as_message(&frame_bytes);
        let msg_payload_type = msg.payload_type();
        if msg_payload_type == PayloadType::NONE {
            return Err(SyncDrawCodecError::InvalidPayload);
        }

        Ok(OwnedMessage {
            buffer: frame_bytes,
            payload_type: msg_payload_type,
        })
    }
}

impl Decoder for SyncDrawCodec {
    type Item = OwnedMessage;
    type Error = SyncDrawCodecError;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        // Read all new data
        self.buffer.extend_from_slice(src);
        src.clear();

        if self.curr_frame_size.is_none() {
            self.try_init_next_frame();
        }

        if let Some(size) = self.curr_frame_size {
            if self.buffer.len() >= size {
                return self.process_frame(size).map(|ok_val| Some(ok_val));
            }
        }

        Ok(None)
    }
}

impl Encoder for SyncDrawCodec {
    type Item = SharedMessage;
    type Error = SyncDrawCodecError;

    fn encode(&mut self, item: Self::Item, buf: &mut BytesMut) -> Result<(), Self::Error> {
        if item.payload_type == PayloadType::NONE {
            return Err(SyncDrawCodecError::InvalidPayload);
        }

        buf.extend_from_slice(&item.buffer);
        Ok(())
    }
}
